-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 27 Jul 2020 pada 04.28
-- Versi server: 10.4.13-MariaDB
-- Versi PHP: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eco-mercy`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `nama` text NOT NULL,
  `deskripsi` text NOT NULL,
  `harga` text NOT NULL,
  `gambar` varchar(125) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nama` text NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  `token` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `nama`, `email`, `password`, `token`) VALUES
(9, 'said', 'said@said.com', 'f5d6a8e4fbb1f496b9ad5d6cc4566546d460b919', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9leGFtcGxlLm9yZyIsImF1ZCI6Imh0dHA6XC9cL2V4YW1wbGUuY29tIiwiaWF0IjoxNTk1NjY4MjczLCJuYmYiOjE1OTU2NjgyODMsImRhdGEiOnsiaWQiOiI5IiwibmFtYSI6InNhaWQiLCJlbWFpbCI6InNhaWRAc2FpZC5jb20ifX0.spshyere5HXS0yNQupn9pEauV4hEBnR8goCaL6yYR94'),
(10, 'aku', 'aku@aku.com', '9f7f7d2974b29f44ba398c41145f2f3c02da24b0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9leGFtcGxlLm9yZyIsImF1ZCI6Imh0dHA6XC9cL2V4YW1wbGUuY29tIiwiaWF0IjoxNTk1NjY5MDE5LCJuYmYiOjE1OTU2NjkwMjksImRhdGEiOnsiaWQiOiIxMCIsIm5hbWEiOiJha3UiLCJlbWFpbCI6ImFrdUBha3UuY29tIn19.JtXa26CxB7WyR7nGvtoF5swvn4rdY3aZtunb0531r9U'),
(11, 'aku', 'aku@aku.coms', '9f7f7d2974b29f44ba398c41145f2f3c02da24b0', ''),
(12, 'aku', 'aku@aku.comyy', '9f7f7d2974b29f44ba398c41145f2f3c02da24b0', ''),
(13, 'aku', 'aku@aku.comyyy', '9f7f7d2974b29f44ba398c41145f2f3c02da24b0', ''),
(14, 'aku', 'aku@aku.comyyye', '9f7f7d2974b29f44ba398c41145f2f3c02da24b0', ''),
(15, 'aku', 'aku@aku.comyyyes', '9f7f7d2974b29f44ba398c41145f2f3c02da24b0', ''),
(16, 'aku', 'aku@aku.comyyyess', '9f7f7d2974b29f44ba398c41145f2f3c02da24b0', ''),
(17, 'aku', 'aku@aku.comyyyesss', '9f7f7d2974b29f44ba398c41145f2f3c02da24b0', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9leGFtcGxlLm9yZyIsImF1ZCI6Imh0dHA6XC9cL2V4YW1wbGUuY29tIiwiaWF0IjoxNTk1Njg4NTg5LCJuYmYiOjE1OTU2ODg1OTksImRhdGEiOnsiaWQiOjE3LCJuYW1hIjoiYWt1IiwiZW1haWwiOiJha3VAYWt1LmNvbXl5eWVzc3MifX0.t_BoM03afgw6a5MEl6PHygE3UQTSAJubamVGfOR6xzA');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
