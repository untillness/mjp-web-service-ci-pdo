<div class="container my-4">
        <div class="row justify-content-center">
            <div class="col-8 card">
               <div class="card-title mt-3 text-center "><h3>Register Here!</h3></div>
               <div class="card-body">
                   <form action="" method="post" class="row">
                   <input type="hidden" id="base" value="<?php echo base_url(); ?>">
                        <div class="form-group col-12">
                            <label for="email">Email</label>
                            <input type="email" placeholder="youremail@email.com" name="email" autocomplete="off" class="form-control bulet" id="email">
                        </div>
                        <div class="form-group col-12">
                            <label for="nama">Full Name</label>
                            <input type="text" placeholder="Donald Trump" name="nama" autocomplete="off" class="form-control bulet" id="nama">
                        </div>
                        <div class="form-group col-lg-6 col-sm-12">
                            <label for="pass">New Password</label>
                            <input type="password" name="password" placeholder="must be 8-character"  class="form-control bulet" id="pass">
                        </div>
                        <div class="form-group col-lg-6 col-sm-12">
                            <label for="rePass">Retype Password</label>
                            <input type="password" name="rePass" placeholder="retype new password"   class="form-control bulet" id="rePass">
                        </div>
                        <div class="col-12">
                            <button type="button" id="register" name="register" class="btn btn-re btn-block">Register</button>
                        </div>
                        <div class="col-12">
                           <small>Do You Have Account ? <a href="<?=base_url() ?>/home/login">Login.</a></small>
                        </div>
                   </form>
               </div>
            </div>
        </div>
    </div>