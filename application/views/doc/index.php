            <div id="content">
                <section>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <h3 class="section-judul">Product</h3>
                            </div>
                            <!-- get -->
                            <div class="col-lg-12">
                                <p class="method"><span>[GET]</span>&nbsp&nbsp&nbspRetrive all Product.</p>
                            </div>
                            <div class="col-lg-12">
                                <p class="desk">You can <b>GET</b> all your product data using the url below or specific
                                    product data by including the id after the <span>product/</span>. You must also send
                                    an <span>X-Auth</span> key that is filled with your token.
                                </p>
                            </div>
                            <div class="col-lg-12">
                                <p>example:</p>
                                <h6>
                                    <?= base_url('product') ?>
                                </h6>
                                <p>or specific produk</p>
                                <h6>
                                    <?= base_url('product/') ?><span class="id">{id}</span>
                                </h6>
                            </div>
                            <div class="col-lg-12">
                                <pre class="script">
{
    "status":1,
    "pesan":"Produk Ditemukan",
    "data":[
            {
            "id":"2",
            "nama":"kaos",
            "deskripsi":"kaos oblong hybest",
            "harga":"2000",
            "gambar":"asda",
            "id_user":"1"}
            ]
}
                                </pre>
                            </div>
                            <!-- end get -->
                            <!-- post -->

                            <div class="col-lg-12">
                                <p class="method"><span>[POST]</span>&nbsp&nbsp&nbspAdd an Product.</p>
                            </div>
                            <div class="col-lg-12">
                                <p class="desk">
                                    You can <b>ADD</b> your product using the link below and send the key
                                    <span>X-Auth</span> filled in your token,<span>nama</span> filled in with product
                                    name,<span>deskripsi</span>
                                    filled in
                                    with product description,<span>harga</span> filled in with product
                                    price,<span>gambar</span>
                                    filled in
                                    product image. For product images, fill in images that have been encoded to base64.
                                </p>
                            </div>
                            <div class="col-lg-12">
                                <p>example:</p>
                                <h6>
                                    <?= base_url('product/add') ?>
                                </h6>
                            </div>
                            <div class="col-lg-12">
                                <pre class="script">
{
    "status":1,
    "pesan":"Data Berhasil Ditambahkan",
    "data":{
        "nama":"celana",
        "deskripsi":"celana panjang",
        "harga":"200000",
        "gambar":"assets\/img\/img-1595556861.jpeg",
        "id_user":1
        }
}
                                </pre>
                            </div>

                            <!-- end post -->
                            <!-- put -->

                            <div class="col-lg-12">
                                <p class="method"><span>[PUT]</span>&nbsp&nbsp&nbspEdit Certain Products.</p>
                            </div>
                            <div class="col-lg-12">
                                <p class="desk">You can <b>EDIT</b> your product using the link below and send the key
                                    <span>X-Auth</span> filled in your token,<span>id</span> filled with product
                                    IDs<span>nama</span> filled in with product
                                    name,<span>deskripsi</span>
                                    filled in
                                    with product description,<span>harga</span> filled in with product
                                    price,<span>gambar</span>
                                    filled in
                                    product image. For product images, fill in images
                                </p>
                            </div>
                            <div class="col-lg-12">
                                <p>example:</p>
                                <h6>
                                    <?= base_url('product/edit') ?>
                                </h6>
                            </div>
                            <div class="col-lg-12">
                                <pre class="script">
{
    "status":1,
    "pesan":"Data Berhasil Diubah",
    "data":{
            "id":"4",
            "nama":"kaos",
            "deskripsi":"kaos lengan panjang",
            "harga":"250000",
            "gambar":"assets\/img\/img-1595557441.jpeg"
            }
}
                                </pre>
                            </div>

                            <!-- end put -->
                            <!-- DELETE -->

                            <div class="col-lg-12">
                                <p class="method"><span>[DELETE]</span>&nbsp&nbsp&nbspDelete Certain Products.</p>
                            </div>
                            <div class="col-lg-12">
                                <p class="desk">You can <b>DELETE</b> your product using the link below and send the key
                                    <span>X-Auth</span> filled in your token,
                                    <span>id</span> that is filled with the product ID to be deleted.
                                </p>
                            </div>
                            <div class="col-lg-12">
                                <p>example:</p>
                                <h6>
                                    <?= base_url('product/delete') ?>
                                </h6>
                            </div>
                            <div class="col-lg-12">
                                <pre class="script">
{
    "status":1,
    "pesan":"Data Berhasil Dihapus",
    "data":[]
}
                                </pre>
                            </div>

                            <!-- end DELETE -->

                        </div>
                    </div>
                </section>
            </div>
            </div>