<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Product extends RestController
{
    public function __construct()
    {
        parent::__construct();

        $this->getMiddleware()->run();
    }
    public function index_get($id = null)
    {
        $idUser = $this->currentUser()->id;
        $Product = $this->Product_model->getProduk($id, $idUser);
        if ($Product) {
            $res = $this->res->send(1, 'Produk Ditemukan', $Product);
            $this->response($res, 200);
        } else {
            $res = $this->res->send(0, 'Produk Tidak Ditemukan');
            return $this->response($res, 200);
        }
    }

    public function uploadImage()
    {
        $imgPay = $this->input->post('gambar');
        $explodeImgPay = explode('data:image/', $imgPay);
        $explodeImgPay = explode(';base64,', $explodeImgPay[1]);
        $imgExt = $explodeImgPay[0];
        $imgPay = $explodeImgPay[1];
        $imgName = 'assets/img/img-' . time() . '.' . $imgExt;

        $file =  file_put_contents('./' . $imgName, base64_decode($imgPay));
        return $imgName;
    }

    public function add_post()
    {

        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');
        $this->form_validation->set_rules('harga', 'Harga', 'required|numeric');
        $this->form_validation->set_rules('gambar', 'Gambar produk', 'required');

        if ($this->form_validation->run() == false) {
            $res = $this->res->send(0, array_values($this->form_validation->error_array())[0]);
            return $this->response($res, 200);
        }


        $data = [
            'nama'      => $this->post('nama'),
            'deskripsi' => $this->post('deskripsi'),
            'harga'     => $this->post('harga'),
            'gambar'    => $this->uploadImage(),
            'id_user'   => $this->currentUser()->id
        ];

        $Product = $this->Product_model->insertProduk($data);
        $res = $this->res->send(1, 'Data Berhasil Ditambahkan', $data);
        return $this->response($res, 200);
    }

    public function changeUploadImage($gambar, $gambarLama)
    {
        if ($imgPay = $gambar) {
            $explodeImgPay = explode('data:image/', $imgPay);
            $explodeImgPay = explode(';base64,', $explodeImgPay[1]);
            $imgExt = $explodeImgPay[0];
            $imgPay = $explodeImgPay[1];

            unlink(FCPATH . $gambarLama);

            $imgName = 'assets/img/img-' . time() . '.' . $imgExt;

            $file =  file_put_contents('./' . $imgName, base64_decode($imgPay));
        }
        return $imgName;
    }

    public function edit_put()
    {
        $this->form_validation->set_data($this->put());

        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');
        $this->form_validation->set_rules('harga', 'Harga', 'required|numeric');

        if ($this->form_validation->run() == false) {
            $res = $this->res->send(0, array_values($this->form_validation->error_array())[0]);
            return $this->response($res, 200);
        }

        $id = $this->put('id');
        $gambar = $this->put('gambar');

        $idUser = $this->currentUser()->id;

        if ($this->Product_model->cekId($id, $idUser) == 0) {
            $res = $this->res->send(0, 'Id Data Tidak Ditemukan');
            return $this->response($res, 200);
        }

        $cekId = $this->Product_model->getProduk($id, $idUser);
        $gambarLama = $cekId['gambar'];

        $data = [
            'id' => $id,
            'nama' => $this->put('nama'),
            'deskripsi' => $this->put('deskripsi'),
            'harga' => $this->put('harga'),
        ];

        if (!empty($gambar)) {
            $data['gambar'] = $this->changeUploadImage($gambar, $gambarLama);
        } else {
            $data['gambar'] = $gambarLama;
        }

        $this->Product_model->updateProduct($id, $data);
        $res = $this->res->send(1, 'Data Berhasil Diubah', $data);
        return $this->response($res, 200);
    }

    public function delete_delete()
    {
        $id = $this->delete('id');
        $idUser = $this->currentUser()->id;

        if ($this->Product_model->cekId($id, $idUser) == 0) {
            $res = $this->res->send(0, 'Id Data Tidak Ditemukan');
            return $this->response($res, 200);
        }

        $this->Product_model->deleteProduct($id, $idUser);

        $res = $this->res->send(1, 'Data Berhasil Dihapus');
        return $this->response($res, 200);
    }
}
