<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['css'] = "";
		$data['title'] = "EcoMercy | Home";
		$this->load->view('template/top', $data);
		$this->load->view('home/index');
		$this->load->view('template/bottom');
	}

	public function login()
	{
		if ($this->sess->isAuth()) {
			return redirect(base_url('home'));
		}

		$data['css'] = "login.css";
		$data['title'] = "EcoMercy | login";
		$this->load->view('template/top', $data);
		$this->load->view('home/login');
		$this->load->view('template/bottom');
	}

	public function register()
	{
		if ($this->sess->isAuth()) {
			return redirect(base_url('home'));
		}
		$data['css'] = "login.css";
		$data['title'] = "EcoMercy | register";
		$this->load->view('template/top', $data);
		$this->load->view('home/register');
		$this->load->view('template/bottom');
	}

	public function logout()
	{
		$this->sess->remove(['isAuth', 'data']);
		redirect(base_url('home/login'));
	}

	public function confirm($token = '')
	{
		$valid = $this->getMiddleware()->validate($token);
		if ($valid != false) {
			$this->sess->set([
				'isAuth' => 1,
				'data' => $valid
			]);

			return redirect(base_url('home'));
		}
	}
}
