<?php

class Res
{

    private $CI;

    function __construct()
    {
        $this->CI = get_instance();
    }

    /**
     * @param $status Pilih status 0 fail | 1 success
     * @param $pesan Tulis pesan ''
     * @param $data Tulis data []
     */
    public function send($status, $pesan, $data = [])
    {
        return [
            'status' => $status,
            'pesan' => $pesan,
            'data' => $data
        ];
    }
}
