<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{
    private $table = 'users';

    public function get_by_email_count($email)
    {
        $data = $this->mpdo->get_where($this->table, ['email' => $email]);
        return $data->count();
    }

    public function create($data)
    {
        return $this->mpdo->insert($this->table, $data);
    }

    public function get_by_email($email)
    {
        $data = $this->mpdo->get_where($this->table, ['email' => $email])->first();
        return $data;
    }

    public function update($id, $token)
    {
        $this->mpdo->where('id', $id);
        $this->mpdo->update($this->table, [
            'token' => $token
        ]);
    }

    public function get_token($email)
    {
        $data = $this->get_by_email($email);
        return $data->token;
    }

    public function check_token($token)
    {
        $get = $this->mpdo->get_where($this->table, ['token' => $token])->count();
        return $get > 0 ? true : false;
    }
}
