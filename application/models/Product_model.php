<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product_model extends CI_Model
{
    private $table = 'products';

    function getProduk($id, $idUser)
    {
        $this->mpdo->where('id_user', $idUser);
        if ($id == null) {
            $product = $this->mpdo->get($this->table)->result();
        } else {
            $this->mpdo->where('id', $id);
            $product = $this->mpdo->get($this->table)->first();
        }
        return $product;
    }

    public function insertProduk($data)
    {
        $this->mpdo->insert($this->table, $data);
    }

    public function cekId($id, $idUser)
    {
        $data = $this->mpdo->get_where($this->table, ['id' => $id, 'id_user' => $idUser])->count();
        return $data;
    }

    public function updateProduct($id, $data)
    {
        $this->mpdo->where('id', $id);
        $this->mpdo->update($this->table, $data);
    }

    public function deleteProduct($id, $idUser)
    {
        $this->mpdo->delete($this->table, [
            'id' => $id,
            'id_user' => $idUser
        ]);
    }
}
