$(document).ready(function () {
  $("input[name=email]").keydown(function (e) {
    enter(e);
  });
  $("input[name=password]").keydown(function (e) {
    enter(e);
  });
  $("input[name=nama]").keydown(function (e) {
    enter(e);
  });
  $("input[name=rePass]").keydown(function (e) {
    enter(e);
  });

  $("#register").click(function () {
    var url = $("#base").val();
    // console.log(url);
    var email = $("input[name=email]").val();
    var password = $("input[name=password]").val();
    var repass = $("input[name=rePass]").val();
    var nama = $("input[name=nama]").val();

    $.ajax({
      url: url + "auth/register",
      method: "post",
      dataType: "json",
      data: {
        email: email,
        password: password,
        rePass: repass,
        nama: nama,
      },
      success: function (data) {
        reg(data);
      },
    });
  });

  $("#login").click(function () {
    var url = $("#base").val();
    var email = $("input[name=email]").val();
    var password = $("input[name=password]").val();

    $.ajax({
      url: url + "auth/login",
      method: "post",
      dataType: "json",
      data: {
        email: email,
        password: password,
      },
      success: function (data) {
        log(data);
        // console.log(data.pesan);
      },
    });
  });

  function reg(res) {
    if (res.status == 1) {
      Swal.fire({
        icon: "success",
        title: "Register Success",
        text: res.pesan,
      }).then(function (result) {
        if (result.value) {
          window.location = "http://localhost/mjp-web-service-ci/home/login";
        }
      });
    } else {
      Swal.fire({
        icon: "error",
        title: "Register Failed",
        text: res.pesan,
      });
    }
  }
  function log(res) {
    if (res.status == 1) {
      Swal.fire({
        icon: "success",
        title: "Login Success",
        text: res.pesan,
      }).then(function (result) {
        if (result.value) {
          window.location =
            "http://localhost/mjp-web-service-ci/home/confirm/" +
            res.data.token;
        }
      });
    } else {
      Swal.fire({
        icon: "error",
        title: "Login Failed",
        text: res.pesan,
      });
    }
  }
  $("#copy").click(function () {
    var value = $("#token").html();
    var input_temp = document.createElement("input");
    input_temp.value = value;
    document.body.appendChild(input_temp);
    input_temp.select();
    document.execCommand("copy");
    document.body.removeChild(input_temp);
    // console.log(value);
    setTimeout(() => {
      $('[data-toggle="tooltip"]').tooltip("hide");
    }, 700);
  });

  $('[data-toggle="tooltip"]').tooltip({
    animated: "fade",
    placement: "top",
    trigger: "click",
  });
  function enter(e) {
    if (e.which == 13) {
      $("#register").click();
      $("#login").click();
      return false;
    }
  }
});
